+++
title = "Taniti Transportation"

subtitle = "Getting to Taniti is easy by air or sea. Learn about all your options for getting to and getting around the island"

header_bg = "/images/hbg.jpg"

[[sub_menu]]
href = "#travel_to"
link_text = "How To Get Here"
[[sub_menu]]
href = "#travel_in"
link_text = "How To Get Around"
+++

