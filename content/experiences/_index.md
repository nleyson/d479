+++
title = "Things To Do In Taniti"

subtitle = "Taniti is home to tropical beaches, untouched rainforest, and volcanic mountains as well as a vibrant local culture. Learn how to enjoy your stay in Taniti."

header_bg = "/images/hbg.jpg"

[[sub_menu]]
href = "#entertainment"
link_text = "Entertainment In Taniti"
[[sub_menu]]
href = "#sights"
link_text = "Sightseeing"
+++

