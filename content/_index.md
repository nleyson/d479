+++
title = "Taniti"
header_bg = "/images/pexels-asad-photo-maldives-3320516.jpg"
[[sub_menu]]
href = "#about"
link_text = "Discover Taniti"
[[sub_menu]]
href = "#travel"
link_text = "How To Get Here"
[[sub_menu]]
href = "#hotels"
link_text = "Where To Stay"
[[sub_menu]]
href = "#entertainment"
link_text = "Things To Do"
+++