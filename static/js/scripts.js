/*!
* Start Bootstrap - Grayscale v7.0.6 (https://startbootstrap.com/theme/grayscale)
* Copyright 2013-2023 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-grayscale/blob/master/LICENSE)
*/
//
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {
    var scrollPos = window.scrollY;
    var navbarCollapseTimeout = undefined;
    var navBarCollapse = function (e) {
        let main_nav_links = document.body.querySelector('#main-nav-links');
        let hidden = !main_nav_links.classList.contains('show')
        let collapsing = main_nav_links.classList.contains('collapsing')

        if (window.scrollY < scrollPos && hidden && !collapsing) {
            console.log('expand')
            new bootstrap.Collapse(main_nav_links, {show: true})
        } else if (window.scrollY > scrollPos && !hidden && !collapsing) {
            console.log("collapse")
            new bootstrap.Collapse(main_nav_links, {hide: true})
        }
        scrollPos = window.scrollY;
        navbarCollapseTimeout = setTimeout(_=> {navbarCollapseTimeout = undefined}, 200)
    }

    // Navbar shrink function
    var navbarShrink = function () {
        const navbarCollapsible = document.body.querySelector('#mainNav');
        if (!navbarCollapsible) {
            return;
        }
        if (window.scrollY === 0) {
            navbarCollapsible.classList.remove('navbar-shrink')
        } else {
            navbarCollapsible.classList.add('navbar-shrink')
        }
    };
    

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);
    document.addEventListener('scroll', navBarCollapse);

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            rootMargin: '0px 0px -40%',
            offset: -5
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function (responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });

    const cookieBox = document.getElementById('js-cookie-box');
    const cookieButton = document.getElementById('js-cookie-button');
    if (!Cookies.get('cookie-box')) {
        cookieBox.classList.remove('cookie-box-hide');
        cookieButton.onclick = function () {
            Cookies.set('cookie-box', true, {
                expires: 2
            });
            cookieBox.classList.add('cookie-box-hide');
        };
    }
});